import os
import time
from it.conjoint.doe.surveydesign import DOE

from MeteorClient import MeteorClient


class Server:
    def __init__(self):
        pass

    def subscribed(self, subscription):
        print('* SUBSCRIBED {}'.format(subscription))

    def unsubscribed(self, subscription):
        print('* UNSUBSCRIBED {}'.format(subscription))

    def added(self, collection, id, fields):
        print('* ADDED {} {}'.format(collection, id))
        for key, value in fields.items():
            print('  - FIELD {} {}'.format(key, value))
        if collection == 'SurveySessions' and 'sessionId' in fields:
            self.insert_survey_questionaire_layout(fields['surveyId'], fields['sessionId'])
        if collection == 'SurveyResults' and 'responseId' in fields:
            self.computeResult(fields['surveyId'])
            # if collection == 'list' you could subscribe to the list here
            # with something like
            # self.client.subscribe('todos', id)
            # all_todos = self.client.find('todos', selector={})
            # print 'Todos: {}'.format(all_todos)

    def insertion_callback(self, error, result):
        if error:
            print(error)
        print('{} inserted successfully'.format(result))

    def insert_survey_questionaire_layout(self, id, sessionId):
        print('find SurveyDefinition for {}'.format(id))
        sd = self.client.find_one('SurveyDefinitions', selector={'_id': id})
        features = sd['features']
        doe = DOE.from_features(features)
        q = doe.generate_survey(questions_count=7)
        doc = {'surveyId': id, 'sessionId': sessionId, 'questionaire': q, 'name': sd['name'],
               'description': sd['description']}
        print('* INSERT {}'.format(doc))
        self.client.call('insertSurvey', [doc], callback=self.insertion_callback)

    def changed(self, collection, id, fields, cleared):
        print('* Published {} {}'.format(collection, id))
        if 'status' in fields:
            self.insert_survey_design_layout(id)
        if 'Surveys' in collection:
            print('Field in Survey changed: {}'.format(fields));

    def insert_survey_design_layout(self, id):
        doe = self.createDOE(id)
        print("design layout:")
        print(doe.layout)
        doc = {'_id': id, 'layout': doe.layout, 'type': "FULLFACTORIAL"}
        self.client.insert('SurveyDesigns', doc)

    def createDOE(self, id):
        print('find SurveyDefinition for {}'.format(id))
        sd = self.client.find_one('SurveyDefinitions', selector={'_id': id})
        features = sd['features']
        design = DOE.from_features(features)
        return design

    def computeResult(self, surveyId):
        results= self.client.find('SurveyResults', selector={'surveyId': surveyId})
        for r in results:
            print("tst")


    def connected(self):
        print('* CONNECTED')

    def subscription_callback(self, error):
        if error:
            print(error)

    def main(self):
        url=os.environ.get('WEBSOCKET_URL')
        print('WEBSOCKET_URL={}'.format(url))
        self.client = MeteorClient(url, auto_reconnect=True, auto_reconnect_timeout=1)
        self.client.on('subscribed', self.subscribed)
        self.client.on('unsubscribed', self.unsubscribed)
        self.client.on('added', self.added)
        self.client.on('removed', self.added)
        self.client.on('changed', self.changed)
        self.client.on('connected', self.connected)

        self.client.connect()
        self.client.subscribe('SurveyDefinitions', callback=self.subscription_callback)
        self.client.subscribe('SurveySessions', callback=self.subscription_callback)
        self.client.subscribe('SurveyResults', callback=self.subscription_callback)
        self.client.subscribe('Surveys', callback=self.subscription_callback)

        # (sort of) hacky way to keep the self.client alive
        # ctrl + c to kill the scriptNyuF8y42e/1

        while True:
            try:
                time.sleep(1)
            except KeyboardInterrupt:
                break
        self.client.unsubscribe('SurveyDefinitions')
        self.client.unsubscribe('SurveySessions')
        self.client.unsubscribe('SurveyResults')
        self.client.unsubscribe('Surveys')
