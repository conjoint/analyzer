import unittest

from datetime import datetime

import patsy
from MeteorClient import MeteorClient
from it.conjoint.analysis.calculate.logistic_regression import LongTable, LogisticRegression, DiscreteChoiceAnalysis


def subscription_callback(error):
    if error:
        print(error)

class DPPIntegrationTest(unittest.TestCase):
    def setUp(self):
        client = MeteorClient('ws://127.0.0.1:3000/websocket', auto_reconnect=True, auto_reconnect_timeout=1)
        client.connect()
        print('* CONNECTION to DPP established')
        client.subscribe('SurveyResults', callback=subscription_callback)
        client.subscribe('AnalysisResults', callback=subscription_callback)
        self.client = client

    def testRegress(self):
        surveyResults = self.client.find('SurveyResults', selector={'surveyId': 'iuJbbmtvgwBm6FiPS'})
        self.assertIsNotNone(surveyResults)
        assert len(surveyResults) > 0, " size is %r " % len(surveyResults)
        colNames=[]
        data ={}

        #remove all where option NONE was choosen
        for s in surveyResults[:]:
            if not s['features']:
                surveyResults.remove(s)
            else:
                print(s)
        pricekey=""
        for f in surveyResults[0]['features']:
            key = f['name']
            colNames.append(key)
            data[key] = []
            if f['usage'] == 'PRICE':
                pricekey=key


        for r in surveyResults:
            for f in r['features']:
                key = f['name']
                if f['type'] == 'NUMERICAL':
                    cell = float(f['value'])
                    data[key].append(cell)
                if f['type'] == 'CATEGORICAL':
                    cell = f['value']
                    data[key].append(cell)

        data['choice'] = []
        for r in surveyResults:
            print(r)
            data['choice'].append(1 if r['chosen'] == 'AS_BEST' else 0)

        for c in colNames:
            print ("column {} is {} must be {}".format(c, len(data[c]), len(surveyResults)))

        print('table {}'.format(data))

        table = LongTable(data, colNames, pricekey)
        regression = LogisticRegression(table)
        dca = DiscreteChoiceAnalysis(regression)
        willi= dca.calculate_willingness_to_pay()
        result={}
        result['surveyId'] = surveyResults[0]['surveyId']
        result['creationDate'] = datetime.now()
        result['name'] = 'test'
        result['willingnessToPay'] = willi
        #dca.create_report()
        for w in willi:
            for f in willi[w]:
                print("{0:10} = {1:16}\t {2:6} $ \t {3:5}".format(w, f, willi[w][f]['willingnessToPay'], willi[w][f]['pvalue']))

        #self.client.insert('AnalysisResults', result)



    def tearDown(self):
        print('* TEARDOWN DPP')
        self.client.unsubscribe('SurveyResults')
        self.client.unsubscribe('AnalysisResults')
        self.client.close()

if __name__ == '__main__':
    unittest.main()


