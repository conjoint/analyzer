import unittest
import patsy
from it.conjoint.analysis.calculate.logistic_regression import LogisticRegression, LongTable


class LogisticRegressionTest(unittest.TestCase):
    def setUp(self):
        data = patsy.demo_data("choice", "feature A", "feature B", "price", nlevels=3)
        data["choice"] = [1 if x == 'choice1' else 0 for x in data["choice"]]
        table = LongTable(data, ['feature A', 'feature B'])
        self.fixture = LogisticRegression(table)

    def testSignificantFeatures(self):
        self.fixture.isSignificant()
        significant_features = self.fixture.getSignificantCoefficients()
        print(significant_features)
        self.assertTrue(True)

if __name__ == '__main__':
    unittest.main()
