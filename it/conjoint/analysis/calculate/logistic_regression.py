import re

import patsy
import statsmodels.formula.api as smf
from patsy.desc import ModelDesc
from patsy.eval import EvalFactor


class LongTable(object):
    """ Datastructure that holds a Table in the Long Format
    """
    def __init__(self, data, rhs_names, pricekey):
        assert len(rhs_names) > 1
        self.pricekey = pricekey
        lhs= [patsy.Term([EvalFactor("choice")])]
        rhs= self.add_predictors(rhs_names)
        desc = ModelDesc(lhs, rhs)
        #desc = self.add_predictors("choice ~ "+numeric_names[0]+" + 0", category_names)
        self.y, self.X = patsy.dmatrices(desc, data)

    def add (self, line):
        return

    @staticmethod
    def add_predictors(extra_predictors):
        desc=[]
        #patsy.Term([EvalFactor("+0")])
        # Using LookupFactor here ensures that everything will work correctly even
        # if one of the column names in extra_columns is named like "weight.in.kg"
        # or "sys.exit()" or "LittleBobbyTables()".
        desc += [patsy.Term([patsy.LookupFactor(p)]) for p in extra_predictors]
        return desc


class LogisticRegression:
    """ Calculates the Logit Regression of the Discrete Choice Experiment
    """
    def __init__(self, longtable):
        self.regression = smf.Logit(longtable.y, longtable.X)
        self.fit = self.regression.fit()
        self.summary = self.fit.summary()
        self.pricekey=longtable.pricekey

    def isSignificant(self):
        p_value = self.fit.llr_pvalue
        return p_value < 0.05

    def getStatisticalSignificance(self):
        return self.fit.llr_pvalue

    def getSignificantCoefficients(self):
        print(self.summary)
        names = self.regression.exog_names
        pvalues = self.fit.pvalues
        coeffs = self.fit.params
        result = {}
        for pval, coef, n in zip(pvalues, coeffs, names):
            s = re.split('\[|\]', n)
            f= s[0]
            if len(s) <2: s.append(s[0])
            a= s[1]
            if a.startswith('T.'): a= a[2:]
            if not f in result:
                result[f]={}
            result[f][a]={'pvalue': pval, 'coefficient': coef}
        return result


class DiscreteChoiceAnalysis(object):

    def __init__ (self, regression):
        self.regression = regression

    def calculate_willingness_to_pay(self):
        pricekey=self.regression.pricekey
        result = {}
        coefs = self.regression.getSignificantCoefficients()
        price = coefs[pricekey][pricekey]['coefficient']
        del coefs[pricekey]
        for cname in coefs:
            for aname in coefs[cname]:
                params = coefs[cname][aname]
                print (params)
                willingnessToPay = params['coefficient'] / abs(price)
                if not cname in result: result[cname] = {}
                if not aname in result[cname]: result[cname][aname]={}
                result[cname][aname]['willingnessToPay'] = '{:.2f}'.format(round(willingnessToPay, 2));
                result[cname][aname]['pvalue']= '{:.3f}'.format(round(params['pvalue'], 3))
        return result



