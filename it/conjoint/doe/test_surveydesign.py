import unittest
from pandas import json

from it.conjoint.doe.surveydesign import DOE


class LogisticRegressionTest(unittest.TestCase):
    def setUp(self):
        self.fixture = DOE.from_features(features_example['features'])

    def testSurvey(self):
        result = self.fixture.generate_survey(questions_count=7)
        print(result)
        self.assertEquals(7, len(result))

    def testRandomNumbers(self):
        result = self.fixture.generate_random_numbers(10)
        #pprint(result)
        self.assertEquals(3, len(result))

    def testFullfact(self):
        from pyDOE import fullfact
        #print(fullfact([1,2,3]))
        pass

if __name__ == '__main__':
    unittest.main()

features_example = json.loads("""{
    "features" : [
		{
			"name" : "Branded Color",
			"attributes" : [
				"Pink",
				"Grey",
				"Mangenta"
			],
			"type" : [
				"CATEGORICAL"
			]
		},
		{
			"name" : "Branded Size",
			"attributes" : [
				"Grande",
				"Venti"
			],
			"type" : [
				"CATEGORICAL"
			]
		},
		{
			"name" : "Price in USD",
			"attributes" : [
				20,
				29.99,
				39
			],
			"type" : [
				"NUMERICAL"
			]
		}
	]}"""
                             )