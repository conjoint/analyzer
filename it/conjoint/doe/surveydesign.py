from pyDOE import *
from enum import Enum
from random import randint
from hashids import Hashids

Method = Enum('Method', 'fullfactorial, fractionalfactorial OMEP')


class DOE:
    """ Handles the design of experiments. Fisher sei dank.
    """

    @classmethod
    def from_features(cls, features, method=Method.fullfactorial):
        "initialize an experimental design with a feature set"
        levels = []
        for f in features:
            levels.append(len(f['attributes']))
        layout = fullfact(levels)
        layout = cls.convert_to_list(layout)
        return cls(features, layout)

    def __init__(self, features, layout):
        self.layout = layout
        self.features = features

    @staticmethod
    def convert_to_list(layout):
        result = []
        for a in layout:
            result.append(list(a))
        return result

    def generate_survey(self, questions_count=7, nalternatives=3, survey_layout=None, survey_features=None):
        """

        :rtype: List of questions
        """
        if survey_layout is None:
            survey_layout = self.layout
        if survey_features is None:
            survey_features = self.features
        survey = []
        for i in range(0, questions_count):
            random_numbers = self.generate_random_numbers(len(survey_layout))
            question = self.generate_question(random_numbers, survey_layout, survey_features)
            survey.append({'alternatives':question, 'bestAlternative': -1})
        return survey

    @staticmethod
    def generate_question(indices, layout, features, nalternatives=3):
        """
        Generates a question in a survey, a question should comprehend nalternatives, plus a negative alternative.
        :param indices: unique indexes to choose a random product configuration from the survey layout
        :param layout: factorial design list of products a, b , c  with [a1, a2, a3, ..]
        :param features: names for f1[a1, b1, c1], f2[...]
        :return: [{f1:a1, f2:a2, f3:a3},
                  {f1:b1, f2:b2, f3:b3},
                  {f1:c1, f2:c2, f3:c3}
                  ]
        """
        product = []
        sample = []
        for naltern in indices:
            sample.append([int(alt) for alt in layout[naltern]])
        for naltern in range(0, nalternatives):
            result = {}
            for nfeature in range(len(features)):
               f = features[nfeature]['attributes'][sample[naltern][nfeature]]
               result[features[nfeature]['name']]=f
            product.append(result)
        product.append({})
        return product

    @staticmethod
    def generate_random_numbers(ndimensions, nalternatives=3):
        arr = []
        while len(arr) < nalternatives:
            randomnumber = randint(0, ndimensions-1)
            found = False
            for i in range(0, len(arr)):
                if arr[i] == randomnumber:
                    found = True
                    break
            if not found:
                arr.append(randomnumber)
        return arr
