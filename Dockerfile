FROM conjoint.it/analyzer-base

COPY . /usr/src/app

EXPOSE 27017

CMD [ "python", "./run.py" ]

